# [Bootstrap](http://getbootstrap.com) Based Page

# About

This is a simple page built with the Bootstrap framework, using LESS, JQuery and Grunt.

# To run this files

Bootstrap uses Grunt for its build system, with convenient methods for working with the framework. It's how we compile our code, run tests, and more.
Installing Grunt

To install Grunt, you must first download and install node.js (which includes npm). npm stands for node packaged modules and is a way to manage development dependencies through node.js.
Then, from the command line:


    Install grunt-cli globally with npm install -g grunt-cli.
    Navigate to the root /bootstrap/ directory, then run npm install. npm will look at the package.json file and automatically install the necessary local dependencies listed there.


When completed, you'll be able to run the various Grunt commands provided from the command line.
Available Grunt commands
grunt dist (Just compile CSS and JavaScript)

Regenerates the /dist/ directory with compiled and minified CSS and JavaScript files. As a Bootstrap user, this is normally the command you want.
grunt watch (Watch)

Watches the Less source files and automatically recompiles them to CSS whenever you save a change.
grunt test (Run tests)

Runs JSHint and runs the QUnit tests headlessly in PhantomJS.
grunt docs (Build & test the docs assets)

Builds and tests CSS, JavaScript, and other assets which are used when running the documentation locally via bundle exec jekyll serve.
grunt (Build absolutely everything and run tests)

Compiles and minifies CSS and JavaScript, builds the documentation website, runs the HTML5 validator against the docs, regenerates the Customizer assets, and more. Requires Jekyll. Usually only necessary if you're hacking on Bootstrap itself.
